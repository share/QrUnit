package com.jmjs.servlets;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jmjs.tools.Md5Code;
import com.jmjs.tools.Tools;

import net.sf.json.JSONObject;

public class QrServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	@SuppressWarnings("deprecation")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");

		response.setContentType("text/html;charset=UTF-8");
		JSONObject json = new JSONObject();

		String path = request.getParameter("path"), //
		filename = request.getParameter("filename"), //
		ticket = request.getParameter("ticket"), //
		swidth = request.getParameter("width"), //
		sheight = request.getParameter("height");

		System.out
				.print(request.getRealPath("/") + filename + swidth + sheight);
		json.put("state", 0);
		if (ticket != null && ticket.length() > 0) {
			int width = 430, height = 430;
			try {
				if (swidth != null && swidth.length() > 0)
					width = Integer.valueOf(swidth);
			} catch (Exception e) {
			}
			try {
				if (sheight != null && sheight.length() > 0)
					height = Integer.valueOf(sheight);
			} catch (Exception e) {
			}
			if (filename == null)
				filename = new Md5Code().getMD5ofStr(ticket);
			if (filename != null && filename.length() < 1)
				filename = new Md5Code().getMD5ofStr(ticket);

			String os = System.getProperty("os.name");
			if (path == null) {
				if (os.toLowerCase().startsWith("win")) {
					path = request.getRealPath("\\");
					path = path.endsWith("\\") ? path : path + "\\";
				} else {
					path = request.getRealPath("/");
					path = path.endsWith("/") ? path : path + "/";
				}
			}
			System.out.println();
			System.out.println(request.getRequestURI());
			System.out.println(request.getRemoteAddr());
			System.out.println(request.getContextPath());
			System.out.println(request.getServletPath());
			System.out.println(request.getLocalAddr());
			System.out.println();
			String name = Tools.createQrImage(path, filename, ticket, width,
					height);// 返回文件名
			if (name.length() > 0) {
				json.put("state", 1);
				json.put("path",
						request.getLocalAddr() + request.getContextPath()
								+ "/" + name);
			}
		}
		PrintWriter out = response.getWriter();
		out.print(json.toString());
		out.flush();
		out.close();
	}

	public static void main(String[] args) throws Exception {
		System.out.println("helllo");
	}
}
