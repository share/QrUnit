package com.jmjs.tools;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * 调试工具类
 * <p>
 * 创建菜单<br/>
 * 获取并生成设备二维码<br/>
 * 设备授权/更新设备属性
 */
public class Tools {

	/**
	 * 二维码图片存放地址
	 */
	static String imagePath = "/tmp/";

	public static void main(String[] args) throws Exception {

		// 1、申请测试账号
		// 2、在config.properties中配置公众平台账号相关信息及token
		// 3、服务端部署，在测试号中配置服务url和token
		// 4、公众平台开发介绍，服务端代码介绍，创建自定义菜单
		// 5、修改demo服务器代码灯泡通信协议为设备自定义协议
		// 6、确定设备id，生成二维码；确定设备参数，对设备进行授权
		// 7、和设备联调

		if (!new File(imagePath).exists()) {
			System.err.println("路径不存在：" + imagePath);
			return;
		}
	    String os = System.getProperty("os.name");  
	    if(os.toLowerCase().startsWith("win")){  
	      System.out.println(os + " can't gunzip");  
	    }  
	    System.out.println(os);
	    
		 //createQrImage(imagePath,
		 //"http://we.qq.com/d/AQAhiF_3PCbpQXY8k-HS-0OY6TUKQf3yPInAoODs",4000,4000);//ok
	}
	
	/**
	 * 获取二维码
	 * @param path
	 * @param ticket
	 * @param width
	 * @param height
	 */
	 /*
	public static final void createQrImage(
			String ticket){
		createQrImage(ticket,430,430);
	}
	*/
	/**
	 * 自定义宽高的二维码，宽高自定义
	 * @param path
	 * @param ticket
	 * @param width
	 * @param height
	 */
	 /*
	public static final void createQrImage(
			String ticket,int width,int height){
		createQrImage(path, new Md5Code().getMD5ofStr(ticket), ticket, width, height);
	}*/
	
	/**
	 * 文件名为ticket的md5，宽高为430
	 * @param path
	 * @param ticket
	 */
	public static final void createQrImage(String path,
			String ticket){
		createQrImage(path, new Md5Code().getMD5ofStr(ticket), ticket, 430, 430);
	}
	
	/**
	 * 自定义宽高的二维码，文件名为ticket的md5
	 * @param path
	 * @param ticket
	 * @param width
	 * @param height
	 */
	 
	public static final void createQrImage(String path,
			String ticket,int width,int height){
		createQrImage(path, new Md5Code().getMD5ofStr(ticket), ticket, width, height);
	}
	
	/**
	 * 自定义二维码的宽高和文件名
	 * @param path
	 * @param filename
	 * @param ticket
	 * @param width
	 * @param height
	 */
	public static final String createQrImage(String path, String filename,
			String ticket,int width,int height){
		//path = path.endsWith("/") ? path : path + "/";
		
		// 二维码的图片格式
		String format = "jpg";
		String fileName = path + filename + "." + format;
		// 设置二维码的参数
		Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
		hints.put(EncodeHintType.CHARACTER_SET, "utf-8");// 编码
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.Q);// 容错率
		try {
			// 生成二维码
			BitMatrix bitMatrix = new MultiFormatWriter().encode(ticket,
					BarcodeFormat.QR_CODE, width, height, hints);
			// 输出图片
			File outputFile = new File(fileName);
			MatrixToImageWriter.writeToFile(bitMatrix, format, outputFile);

			System.out.println(filename + "(" + filename.length()
						+ ")" + "，ticket：" + ticket + "(" + ticket.length()
						+ ")" + "，生成二维码图片：" + fileName);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		return filename + "." + format;
	}

	/**
	 * 使用zxing库生成二维码图片，宽高为430
	 */
	public static void createQrImage(String path, String filename,
			String ticket) {
		createQrImage(path, filename, ticket, 430, 430);
	}

}
